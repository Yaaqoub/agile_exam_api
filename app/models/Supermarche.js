module.exports = function(sequelize, Sequelize) {

    let Supermarche = sequelize.define('supermarche', {

        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1,
            primaryKey: true
        },

        latitude: {
            type: Sequelize.DECIMAL,
            notEmpty: true
        },

        longitude: {
            type: Sequelize.DECIMAL,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Supermarche;
};