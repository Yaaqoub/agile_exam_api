module.exports = function(sequelize, Sequelize) {

    let Client = sequelize.define('client', {

        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1,
            primaryKey: true
        },

        Nom: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        Prenom: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        Adresse: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        Tel: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Client;
};