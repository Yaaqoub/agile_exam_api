module.exports = function(db) {

    db.produit.belongsTo(db.supermarche, {foreignKey: 'supermarche_id'});

    db.produit.belongsToMany(db.client, {through: 'Commande', foreignKey: 'produit_id', as:'produitHasClient'});
    db.client.belongsToMany(db.produit, {through: 'Commande', foreignKey: 'client_id', as:'clientHasProduit'});
};