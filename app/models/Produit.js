module.exports = function(sequelize, Sequelize) {

    let Produit = sequelize.define('produit', {

        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1,
            primaryKey: true
        },

        Nom: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        Description: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        Prix: {
            type: Sequelize.DECIMAL,
            notEmpty: true
        },

        Image: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Produit;
};