let fs = require("fs");
let path = require("path");
let data = {};

module.exports = function(models) {

    let Produit = models.produit;
    let Supermarche = models.supermarche;

    let produitArray = [{
        Nom: 'produit1',
        Description: 'Description produit1',
        Prix: '100',
        Image: 'https://toppng.com/public/uploads/preview/sandwich-11528331717gis1u7l43u.png',
        supermarche: {
            latitude: '48.867030',
            longitude: '2.379584'
        }
    },{
        Nom: 'produit2',
        Description: 'Description produit2',
        Prix: '50',
        Image: 'http://www.pngpix.com/wp-content/uploads/2016/10/PNGPIX-COM-Sandwich-PNG-Transparent-Image-4-500x369.png',
        supermarche: {
            latitude: '48.866066',
            longitude: '2.377030'
        }
    },{
        Nom: 'produit3',
        Description: 'Description produit3',
        Prix: '20',
        Image: 'http://pngimage.net/wp-content/uploads/2018/06/salades-png-1.png',
        supermarche: {
            latitude: '48.867315',
            longitude: '2.373146'
        }
    },{
        Nom: 'produit4',
        Description: 'Description produit4',
        Prix: '35',
        Image: 'http://miam-images.m.i.pic.centerblog.net/4871c2e3.png',
        supermarche: {
            latitude: '48.865610',
            longitude: '2.367890'
        }
    }];
/*
    let supermarcheArray = [{
        latitude: '48.867030',
        longitude: '2.379584'
    },{
        latitude: '48.866066',
        longitude: '2.377030'
    },{
        latitude: '48.867315',
        longitude: '2.373146'
    },{
        latitude: '48.865610',
        longitude: '2.367890'
    }];*/

   /* fs.readdirSync(path.join(__dirname)).filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== "generatePasswords.js");
    }).forEach(function(file) {
        let prop = camelCase(file.slice(0, -3));
        let Resource = require(`./${file}`);
        this[prop] = Resource;
    });
*/
    /*produitArray.forEach((theProduct) => {
        Produit.create(theProduct).then(function () {
        });
    });*/

    for (let i = 0; i < 4; i++) {
        Produit.create(produitArray[i], {
            include: [{
                model: Supermarche
            }]
        }).then(function () {
        });
    }

    /*supermarcheArray.forEach((theMarket) => {
        Supermarche.create(theMarket).then(function () {
        });
    });*/
};