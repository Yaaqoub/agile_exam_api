var exports = module.exports = {};

let models = require("../models/index"),
    Produit = models.produit,
    Supermarche = models.supermarche;

exports.produitList = function(req, res) {

    Produit.findAll({
        include: { model: Supermarche }
    }).then((theProduct) => {
        res.json(theProduct);
    });
};