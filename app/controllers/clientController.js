var exports = module.exports = {};

let models = require("../models/index"),
    Client = models.client;

exports.listClients = function(req, res) {

    Client.findAll().then((theClients) => {
        res.json(theClients);
    });
};

exports.createClient = function(req, res) {

    if (req.query.nom && req.query.prenom && req.query.adresse && req.query.tel) {
        Client.create({
            Nom: req.query.nom,
            Prenom: req.query.prenom,
            Adresse: req.query.adresse,
            Tel: req.query.tel
        }).then(function () {
            res.json({
                message: 'success'
            });
        });
    } else {
        res.json({
            message: 'failure'
        });
    }
};