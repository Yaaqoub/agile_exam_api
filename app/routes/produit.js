let produitController = require('../controllers/produitController');

module.exports = function(app) {

    /**
     * (GET Method)
     */
    app.get('/produit_list', produitController.produitList);
};