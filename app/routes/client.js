let clientController = require('../controllers/clientController');

module.exports = function(app) {

    /**
     * (GET Method)
     */
    app.get('/client_list', clientController.listClients);

    app.get('/new_client', clientController.createClient);
};